<?php

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    public function findUserByUsername($username)
    {
        return $this->findOneBy([
            'username' => $username
        ]);
    }

    public function findUserByEmail($email)
    {
        return $this->findOneBy([
            'email' => $email
        ]);
    }
}