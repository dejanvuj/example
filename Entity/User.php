<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;

use Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\Table(name="xxx")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getKeys()
    {
        return $this->keys;
    }

    /**
     * @param mixed $keys
     */
    public function setKeys($keys)
    {
        $this->keys = $keys;
    }

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=50, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="array")
     */
    private $roles;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255, unique=true, nullable=true)
     * @Assert\Email()
     */
    private $email;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $phoneNumber;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $birth;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="integer")
     */
    private $userReferrer = 0;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isVerified = false;

    /**
     * @ORM\Column(type="integer")
     */
    private $userKeys = 3;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $placeOfBirth;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $postCode;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $country;

    /**
     * @ORM\OneToMany(targetEntity="Key", mappedBy="user")
     */
    private $keys;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $fatherFirstName;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $fatherLastName;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $fatherDateBirth;

    /**
     * @return mixed
     */
    public function getFatherDateBirth()
    {
        return $this->fatherDateBirth;
    }

    /**
     * @param mixed $fatherDateBirth
     */
    public function setFatherDateBirth($fatherDateBirth)
    {
        $this->fatherDateBirth = $fatherDateBirth;
    }

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $motherFirstName;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $motherLastName;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $motherDateBirth;

    public function __construct()
    {
        $this->roles = ['ROLE_USER'];
        $this->keys = new ArrayCollection();
    }

    public function getRoles()
    {
        return $this->roles;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getSalt()
    {
        return null;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function eraseCredentials()
    {
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param mixed $phoneNumber
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return mixed
     */
    public function getBirth()
    {
        return $this->birth;
    }

    /**
     * @param mixed $birth
     */
    public function setBirth($birth)
    {
        $this->birth = $birth;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getUserReferrer()
    {
        return $this->userReferrer;
    }

    /**
     * @param mixed $referrer
     */
    public function setUserReferrer($userReferrer)
    {
        $this->userReferrer = $userReferrer;
    }

    /**
     * @return mixed
     */
    public function getUserKeys()
    {
        return $this->userKeys;
    }

    /**
     * @param mixed $keys
     */
    public function setUserKeys($userKeys)
    {
        $this->userKeys = $userKeys;
    }

    /**
     * @return bool
     */
    public function isVerified()
    {
        return $this->isVerified;
    }

    /**
     * @param bool $isVerified
     */
    public function setIsVerified($isVerified)
    {
        $this->isVerified = $isVerified;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getPlaceOfBirth()
    {
        return $this->placeOfBirth;
    }

    /**
     * @param mixed $placeOfBirth
     */
    public function setPlaceOfBirth($placeOfBirth)
    {
        $this->placeOfBirth = $placeOfBirth;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getPostCode()
    {
        return $this->postCode;
    }

    /**
     * @param mixed $postCode
     */
    public function setPostCode($postCode)
    {
        $this->postCode = $postCode;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getFatherFirstName()
    {
        return $this->fatherFirstName;
    }

    /**
     * @param mixed $fatherFirstName
     */
    public function setFatherFirstName($fatherFirstName)
    {
        $this->fatherFirstName = $fatherFirstName;
    }

    /**
     * @return mixed
     */
    public function getFatherLastName()
    {
        return $this->fatherLastName;
    }

    /**
     * @param mixed $fatherLastName
     */
    public function setFatherLastName($fatherLastName)
    {
        $this->fatherLastName = $fatherLastName;
    }

    /**
     * @return mixed
     */
    public function getMotherFirstName()
    {
        return $this->motherFirstName;
    }

    /**
     * @param mixed $motherFirstName
     */
    public function setMotherFirstName($motherFirstName)
    {
        $this->motherFirstName = $motherFirstName;
    }

    /**
     * @return mixed
     */
    public function getMotherLastName()
    {
        return $this->motherLastName;
    }

    /**
     * @param mixed $motherLastName
     */
    public function setMotherLastName($motherLastName)
    {
        $this->motherLastName = $motherLastName;
    }

    /**
     * @return mixed
     */
    public function getMotherDateBirth()
    {
        return $this->motherDateBirth;
    }

    /**
     * @param mixed $motherDateBirth
     */
    public function setMotherDateBirth($motherDateBirth)
    {
        $this->motherDateBirth = $motherDateBirth;
    }
}