<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\User;
use AppBundle\Form\TokenType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class TokenController extends Controller
{
    /**
     * @Route("/api/login", name="xxx_login", methods={"POST"})
     */
    public function newTokenAction(Request $request)
    {
        if ($request->getContentType() === "json") {

            $data = json_decode($request->getContent(), true);
            $userData = new User();
            // Check data with form
            $form = $this->createForm(TokenType::class, $userData);
            $form->submit($data);

            if (!$form->isValid()) {
                $errors = $this->getErrorsFromForm($form);
                $data = [
                    'type' => 'message_error',
                    'title' => 'There was a validation error',
                    'errors' => $errors,
                    'status' => false
                ];

                return new JsonResponse($data, 400);
            }

            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository(User::class)
                ->findUserByUsername($data['username']);

            if (!$user) {
                return new JsonResponse([
                    'type' => 'message_error',
                    'title' => 'Such user does not exist',
                    'status' => false
                ], 404);
            }

            $isValid = $this->get('security.password_encoder')
                ->isPasswordValid($user, $data['password']);

            if (!$isValid) {
                return new JsonResponse([
                    'type' => 'message_error',
                    'title' => 'Bad credentials',
                    'status' => false
                ], 400);
            }

            $token = $this->get('lexik_jwt_authentication.encoder')
                ->encode([
                    'username' => $data['username']
                ]);

            return new JsonResponse([
                'token' => $token,
            ]);
        }

        return new JsonResponse('Unsupported Media Type', 415);
    }

    private function getErrorsFromForm(FormInterface $form)
    {
        $errors = [];
        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }

        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = $this->getErrorsFromForm($childForm)) {
                    $errors[$childForm->getName()] = $childErrors;
                }
            }
        }

        return $errors;
    }
}