<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Key;
use AppBundle\Entity\User;
use AppBundle\Form\RegisterProfileType;
use AppBundle\Form\RegisterType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends Controller
{
    /**
     * @Route("/api/register/{key}", name="xxx_register", methods={"POST"})
     */
    public function registerAction(Request $request, $key)
    {
        if ($request->getContentType() === "json") {
            // Check if key is still valid
            $em = $this->getDoctrine()->getManager();
            $isValidKey = $em->getRepository(Key::class)
                ->isKeyValid($key);

            if (!$isValidKey) {
                return new JsonResponse([
                    'type' => 'message_error',
                    'title' => 'Key do not exist or not valid',
                    'status' => false
                ], 400);
            }

            $user = new User();
            $data = json_decode($request->getContent(), true);

            $form = $this->createForm(RegisterType::class, $user);
            $form->submit($data);

            if (!$form->isValid()) {
                $errors = $this->getErrorsFromForm($form);
                $data = [
                    'type' => 'validation_error',
                    'title' => 'There was a validation error',
                    'errors' => $errors,
                    'status' => false
                ];

                return new JsonResponse($data, 400);
            }

            $usernameExist = $em->getRepository(User::class)
                ->findUserByUsername($data['username']);

            if ($usernameExist) {
                $data = [
                    'type' => 'message_error',
                    'title' => 'This username is already taken.',
                    'status' => false
                ];

                return new JsonResponse($data, 400);
            }

            if (strlen($data['password']) < 6) {
                $data = [
                    'type' => 'message_error',
                    'title' => 'Minimum number of characters is 6.',
                    'status' => false
                ];

                return new JsonResponse($data, 400);
            }

            $encodedPassword = $this->get('security.password_encoder')
                ->encodePassword($user, $data['password']);
            $user->setPassword($encodedPassword);


            $em->persist($user);
            $em->flush();

            return new JsonResponse([
                'type' => 'message_success',
                'title' => 'Successfully registered',
                'status' => true
            ], 201);
        }
        // TODO: automate api errors
        return new JsonResponse('Unsupported Media Type', 415);
    }

    /**
     * @Route("/api/register/{key}/{username}", name="xxx_register_update", methods={"UPDATE", "PUT"})
     */
    public function updateProfile($key, $username, Request $request)
    {
        if ($request->getContentType() === "json") {
            $data = json_decode($request->getContent(), true);

            $form = $this->createForm(RegisterProfileType::class);
            $form->submit($data);

            if (!$form->isValid()) {
                $errors = $this->getErrorsFromForm($form);
                $data = [
                    'type' => 'validation_error',
                    'title' => 'There was a validation error',
                    'errors' => $errors,
                    'status' => false
                ];

                return new JsonResponse($data, 400);
            }

            $em = $this->getDoctrine()->getManager();
            $isValidKey = $em->getRepository(Key::class)
                ->isKeyValid($key);

            if (!$isValidKey) {
                return new JsonResponse([
                    'type' => 'message_error',
                    'title' => 'Key do not exist or not valid',
                    'status' => false
                ], 400);
            }

            $findKey = $em->getRepository(Key::class)
                ->findOneBy(['code'=>$key]);

            if (!$findKey) {
                throw $this->createNotFoundException(
                    'No key found matching'
                );
            }

            if ($findKey->getIsUsed()) {
                return new JsonResponse(['type'=>'messge_error','title'=>'Key is used already','status'=>false], 400);
            }

            $user = $em->getRepository(User::class)
                ->findUserByUsername($username);

            if (!$user) {
                return new JsonResponse([
                    'type' => 'message_error',
                    'title' => 'Such user does not exist',
                    'status' => false
                ], 404);
            }


            $emailExist = $em->getRepository(User::class)
                ->findUserByEmail($data['email']);

            if ($emailExist) {
                $data = [
                    'type' => 'message_error',
                    'title' => 'Email is already taken',
                    'status' => false
                ];

                return new JsonResponse($data, 400);
            }

            $birth = new \DateTime($data['birth']);
            $fatherBirth = new \DateTime($data['fatherDateBirth']);
            $motherBirth = new \DateTime($data['motherDateBirth']);
            $user->setFirstName($data['firstName']);
            $user->setLastName($data['lastName']);
            $user->setEmail($data['email']);
            $user->setBirth($birth);
            $user->setPhoneNumber($data['phoneNumber']);
            $user->setAddress($data['address']);
            $user->setPlaceOfBirth($data['placeOfBirth']);
            $user->setCity($data['city']);
            $user->setPostCode($data['postCode']);
            $user->setCountry($data['country']);
            $user->setFatherFirstName($data['fatherFirstName']);
            $user->setFatherLastName($data['fatherLastName']);
            $user->setFatherDateBirth($fatherBirth);
            $user->setMotherFirstName($data['motherFirstName']);
            $user->setMotherLastName($data['motherLastName']);
            $user->setMotherDateBirth($motherBirth);
            $em->flush();

            // Set key to be used
            $findKey->setIsUsed("true");
            $em->flush();

            // Prepare user response
            $response = [
                "type"=>"message_success",
                "title"=>"Update successfully",
                "status"=>true,
            ];

            return new JsonResponse($response, 200);
        }

        return new JsonResponse('Unsupported Media Type', 415);
    }

    private function getErrorsFromForm(FormInterface $form)
    {
        $errors = [];
        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }

        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = $this->getErrorsFromForm($childForm)) {
                    $errors[$childForm->getName()] = $childErrors;
                }
            }
        }

        return $errors;
    }
}